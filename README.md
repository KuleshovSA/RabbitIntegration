# Интеграция 1С и RabbitMQ
Используется для отправки и получения данных из брокера сообщений RabbitMQ с помощью одной из двух внешних Native компонент:
- PinkRabbitMQ (https://github.com/BITERP/PinkRabbitMQ)
- 1C2RMQ (https://sbpg.atlassian.net/wiki/spaces/1C2RMQ/overview)

### Программный интерфейс 1C представлен методами:
- ИнтеграцияRabbitСервер.ОтправитьСообщенияRabbit(..)
- ИнтеграцияRabbitСервер.ПолучитьСообщенияRabbit(...)

### Запуск RabbitMQ в Docker:
```
docker-compose -f dockers/docker-compose.yml up -d
```
